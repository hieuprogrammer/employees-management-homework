/* Using JavaScript DOM to interact with user input data */
// document.querySelector(`#btn-xac-nhan`).onclick = function() {
//     console.log(`TESTING onClick() function call.`);

//     var maNhanVien = document.querySelector(`#ma-nhan-vien`).value;
//     var tenNhanVien = document.querySelector(`#ten-nhan-vien`).value;
//     var chucVu = document.querySelector(`#chuc-vu`).value;
//     var luongCoBan = document.querySelector(`#luong-co-ban`).value;
//     var soGioLamTrongThang = parseInt(document.querySelector(`#so-gio-lam-trong-thang`).value);

//     console.log(`${maNhanVien}`);
//     console.log(`${tenNhanVien}`);
//     console.log(`${chucVu}`);
//     console.log(`${luongCoBan}`);
//     console.log(`${soGioLamTrongThang}`);

//     document.querySelector(`#txt-ma-nhan-vien`).innerHTML = maNhanVien;
//     document.querySelector(`#txt-ten-nhan-vien`).innerHTML = tenNhanVien;
//     document.querySelector(`#txt-chuc-vu`).innerHTML = chucVu;
//     document.querySelector(`#txt-tong-luong`).innerHTML = `$${tinhLuong(luongCoBan, soGioLamTrongThang)}`;
//     document.querySelector(`#txt-xep-loai-trong-thang`).innerHTML = xepLoai(soGioLamTrongThang);
// };

// function tinhLuong(luongCoBan, soGioLamTrongThang) {
//     return luongCoBan * soGioLamTrongThang;
// }

// function xepLoai(soGioLamTrongThang) {
//     var innerHTML = ``;
//     if (soGioLamTrongThang < 0) {
//         innerHTML += `Số giờ làm không thể là số âm.`;
//     } else if (soGioLamTrongThang >= 0 && soGioLamTrongThang < 80) {
//         innerHTML += `⭐`;
//     } else if (soGioLamTrongThang >= 80 && soGioLamTrongThang < 160) {
//         innerHTML += `⭐⭐`;
//     } else if (soGioLamTrongThang >= 160 && soGioLamTrongThang < 240) {
//         innerHTML += `⭐⭐⭐`;
//     } else if (soGioLamTrongThang >= 240 && soGioLamTrongThang < 320) {
//         innerHTML += `⭐⭐⭐⭐`;
//     } else if (soGioLamTrongThang >= 320 && soGioLamTrongThang <= Number.MAX_VALUE) {
//         innerHTML += `⭐⭐⭐⭐⭐`;
//     } else {
//         innerHTML += `⭐⭐⭐⭐⭐ Ờ-mây-zing! Gút-chóp! ⭐⭐⭐⭐⭐`;
//     }

//     return innerHTML;
// }

/* Using JavaScript objects to interact with user input data */
var nhanVien = {
    maNhanVien: ``,
    tenNhanVien: ``,
    chucVu: ``,
    luongCoBan: ``,
    soGioLamTrongThang: ``,

    tinhLuong: function (luongCoBan, soGioLamTrongThang) {
        return luongCoBan * soGioLamTrongThang;
    },
    
    xepLoai: function (soGioLamTrongThang) {
        var innerHTML = ``;
        if (soGioLamTrongThang < 0) {
            innerHTML += `Số giờ làm không thể là số âm.`;
        } else if (soGioLamTrongThang >= 0 && soGioLamTrongThang < 80) {
            innerHTML += `⭐`;
        } else if (soGioLamTrongThang >= 80 && soGioLamTrongThang < 160) {
            innerHTML += `⭐⭐`;
        } else if (soGioLamTrongThang >= 160 && soGioLamTrongThang < 240) {
            innerHTML += `⭐⭐⭐`;
        } else if (soGioLamTrongThang >= 240 && soGioLamTrongThang < 320) {
            innerHTML += `⭐⭐⭐⭐`;
        } else if (soGioLamTrongThang >= 320 && soGioLamTrongThang <= Number.MAX_VALUE) {
            innerHTML += `⭐⭐⭐⭐⭐`;
        } else {
            innerHTML += `⭐⭐⭐⭐⭐ Ờ-mây-zing! Gút-chóp! ⭐⭐⭐⭐⭐`;
        }
    
        return innerHTML;
    }
}

document.querySelector(`#btn-xac-nhan`).onclick = function() {
    nhanVien.maNhanVien = document.querySelector(`#ma-nhan-vien`).value;
    nhanVien.tenNhanVien = document.querySelector(`#ten-nhan-vien`).value;
    nhanVien.chucVu = document.querySelector(`#chuc-vu`).value;
    nhanVien.luongCoBan = document.querySelector(`#luong-co-ban`).value;
    nhanVien.soGioLamTrongThang = parseInt(document.querySelector(`#so-gio-lam-trong-thang`).value);

    document.querySelector(`#txt-ma-nhan-vien`).innerHTML = nhanVien.maNhanVien;
    document.querySelector(`#txt-ten-nhan-vien`).innerHTML = nhanVien.tenNhanVien;
    document.querySelector(`#txt-chuc-vu`).innerHTML = nhanVien.chucVu;
    document.querySelector(`#txt-tong-luong`).innerHTML = `$${nhanVien.tinhLuong(nhanVien.luongCoBan, nhanVien.soGioLamTrongThang)}`;
    document.querySelector(`#txt-xep-loai-trong-thang`).innerHTML = nhanVien.xepLoai(nhanVien.soGioLamTrongThang);
}